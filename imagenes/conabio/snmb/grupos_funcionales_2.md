
Reporte
=======
  

# Motivación

- **Persona u organización que desarrolla el experimento**: Coordinación de Ecoinformática - CONABIO
- **Resumen**: Se desea realizar un agrupamiento de especies según algunos criterios funcionales relacionados con el tipo de alimentación, la talla de los especímenes, tipo de hábitat, etc., con el fin de verificar si este tipo de agrupamientos permite realizar una mejor clasificación que los agrupamientos tradicionales por categorías taxonómicas.
- **Propuesta de solución**: Se utiliza el conjunto de fotos etiquetadas manualmente con recuadros del SNMB. Se toman estos recuadros para generar recortes de cada individuo y se crea un dataset con el que se entrena un modelo de clasificación de imágenes (Inception Resnet v2). El modelo entrenado es evaluado con 16% de las fotos de este conjunto, las cuales no fueron vistas en el proceso de entrenamiento por el modelo. El dataset está muy desbalanceado, por lo que sería normal que el modelo tuviera mejor rendimiento para las categorías con un mayor número de muestras.
- **Tipo de problema**: Clasificación multi-clase
  

# Dataset
  

## Información del conjunto de datos

- **Collección**: snmb
- **Versión**: 2020, detection-bboxes
- **URL**: http://www.conabio.gob.mx/snmb_files/conabio_ml_collections/images/snmb_2020_detection-bboxes.tar.gz
  

## Información de preprocesamiento y aumento de datos


Operaciones de preprocesamiento y aumento de datos realizadas en los conjuntos de datos.

*No se registró información de preprocesamiento para el conjunto de datos.*
- Factor de reescala: 0.00392156862745098
- Voltear aleatoriamente las entradas horizontalmente: True
- Rango en grados para rotaciones aleatorias: 20
- Rango para zoom aleatorio: 0.15
- Rango para desplazamientos horizontales aleatorios: 0.1
- Rango para desplazamientos verticales aleatorios: 0.1
- Intensidad de corte (ángulo de corte en sentido antihorario en grados): 0.15
- Las dimensiones a las que todas las imágenes encontradas serán redimensionadas (alto, ancho): [299, 299]
- Tamaño de los lotes de datos: 32
  

## Distribución del conjunto de datos
  


A continuación se muestran las 10 etiquetas con más elementos:  

|Etiquetas|Muestras|
| :---: | :---: |
|HPTL|7227|
|Carr|5368|
|HRTL|2853|
|PAc|2523|
|IFS|2372|
|HPTM|1973|
|COTL|1586|
|COTB|1498|
|GFS|1439|
|FrGSCM|1354|


<details>

<summary>Para ver los 50 elementos restantes, haga clic aquí</summary>  
  

|Etiquetas|Muestras|
| :---: | :---: |
|FrOSCB|1160|
|FFSA|652|
|COSCB|604|
|OFS|608|
|FrOSFM|561|
|FrHTL|551|
|FrGTB|453|
|COSCM|411|
|IOSF B|406|
|IFF|309|
|FrOSCM|239|
|CSCB|163|
|HPTB|147|
|FFS|126|
|OA|90|
|IFCA|91|
|HFS|77|
|FA|73|
|IOSCSM|71|
|CSCL|63|
|IOSFM|59|
|IOTB|54|
|ICABD|44|
|IOSCM|33|
|CCT|39|
|FrOTL|31|
|IOTM|24|
|GSFSM|19|
|CTL|19|
|CSFB|16|
|MSCB|18|
|SVSM|15|
|FrTSM|14|
|GA|12|
|IOAM|10|
|FrOSCSM|9|
|HAc|10|
|FrHAB|7|
|NeA|7|
|CTB|6|
|IAVS|5|
|GTSM|4|
|FrGTSM|4|
|CSFM|3|
|FrOAM|3|
|FrOSFSM|3|
|FrOTB|1|
|FrGASM|1|
|PSQB|1|
|FrGSFSM|1|


</details>  
  

## Particiones en los conjuntos de datos
  

### Porcentajes
  

|Entrenamiento|Prueba|
| :---: | :---: |
|84%|16%|
  

### Muestras
  


A continuación se muestran las 10 etiquetas con más elementos:  

|Etiquetas|Entrenamiento|Prueba|
| :---: | :---: | :---: |
|HPTL|5859|1368|
|Carr|5173|195|
|HRTL|2219|634|
|PAc|1995|528|
|IFS|1937|435|
|HPTM|1646|327|
|COTL|1345|241|
|COTB|1199|299|
|GFS|1172|267|
|FrGSCM|1080|274|


<details>

<summary>Para ver los 50 elementos restantes, haga clic aquí</summary>  
  

|Etiquetas|Entrenamiento|Prueba|
| :---: | :---: | :---: |
|FrOSCB|946|214|
|FFSA|526|126|
|COSCB|516|88|
|OFS|500|108|
|FrOSFM|488|73|
|FrHTL|430|121|
|FrGTB|368|85|
|COSCM|341|70|
|IOSF B|333|73|
|IFF|255|54|
|FrOSCM|197|42|
|CSCB|135|28|
|HPTB|120|27|
|FFS|104|22|
|OA|77|13|
|IFCA|73|18|
|HFS|66|11|
|FA|66|7|
|IOSCSM|61|10|
|CSCL|53|10|
|IOSFM|49|10|
|IOTB|45|9|
|ICABD|38|6|
|IOSCM|33|0|
|CCT|30|9|
|FrOTL|26|5|
|IOTM|21|3|
|GSFSM|19|0|
|CTL|16|3|
|CSFB|16|0|
|MSCB|16|2|
|SVSM|14|1|
|FrTSM|14|0|
|GA|11|1|
|IOAM|10|0|
|FrOSCSM|9|0|
|HAc|9|1|
|FrHAB|7|0|
|NeA|6|1|
|CTB|6|0|
|IAVS|5|0|
|GTSM|4|0|
|FrGTSM|4|0|
|CSFM|3|0|
|FrOAM|3|0|
|FrOSFSM|3|0|
|FrOTB|1|0|
|FrGASM|1|0|
|PSQB|1|0|
|FrGSFSM|1|0|


</details>  
  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/plot_samples_per_class_trunc.png)  
[Para ver un diagrama con la distribución de todas las etiquetas, haga clic aquí.](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/plot_samples_per_class.png)  

### Criterios adicionales


*No se registraron criterios adicionales para el conjunto de datos.*  

# Modelo
  

## Datos del modelo

- **Nombre del modelo**: Modelo Inception
- **Tipo de modelo**: Red neuronal convolucional y una variante de Inception sin conexiones residuales. La red preentrenada puede clasificar imágenes en 1000 categorías de objetos.
- **Datos de entrada**: La red tiene un tamaño de entrada de imagen de 299 por 299.
- **Datos de salida**: La capa de salida es softmax, lo que significa que tiene un número predefinido de neuronas, cada una está definida para una clase específica.
  

## Notas adicionales

- **Usuarios principales**: Los investigadores dedicados al monitoreo de la vida silvestre a través de fotografías de cámaras trampa.
- **Usos principales**: Realizar una clasificación semi-automática de especies agrupadas según criterios funcionales.
- **Usos no previstos**: Clasificación en niveles taxonómicos. Clasificación de especies de más de una categoría dentro de la misma imagen. Clasificación de animales en fotografías que no son de cámaras trampa.
  

# Evaluación
  

## Metodología
  

### Predicción en la clasificación

- **Clasificación**: Los modelos de clasificación producen la probabilidad para cada una de las categorías en un conjunto bien definido de clases. Generalmente, se toma la categoría con la mayor probabilidad y esa etiqueta se asigna a toda la imagen.
  

#### Método de clasificación


A partir del conjunto de predicciones que genera el modelo, se consideran las N con la probabilidad más alta y se asigna una etiqueta a cada muestra para cada una de estas clases.
En el caso N > 1, se dice que es una clasificación de multi-etiqueta, ya que cada muestra tendrá múltiples etiquetas.
En el caso N = 1, se dice que es una clasificación multi-clase, ya que cada muestra tendrá una de un conjunto de varias etiquetas. En el caso especial donde solo hay 2 clases y solo se asigna una a cada muestra, se conoce como clasificación binaria.  

##### Umbrales de decisión utilizados en la clasificación

- Número máximo de predicciones que se tuvieron en cuenta.: 5
  
![Predicción en la clasificación](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/docs/methodology_classification_2_en.png)  

## Resultados


Resumen que incluye gráficas, ejemplos de clasificación y una interpretación de los resultados.  

### Gráficas


Gráficas o alguna interpretación gráfica, como la matriz de confusión, que ayudan a tener una interpretación rápida de los resultados.

Las métricas para las 10 categorías con más elementos se muestran a continuación:  

#### Gráficas de evaluación multi-clase
  

##### Gráfica para la evaluación global del modelo.
  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/MULTICLASS-one_class-trunc.png)  

##### Gráficas para la evaluación por clase del modelo.


Gráfica de Precisión  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/MULTICLASS-per_class-precision-trunc.png)

Gráfica de recuperación (Recall)  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/MULTICLASS-per_class-recall-trunc.png)

Gráfica de Score f1  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/MULTICLASS-per_class-f1_score-trunc.png)

Para ver las gráficas con las métricas de todas las categorías, haga clic en los enlaces a continuación:  
[MULTICLASS-per_class-precision](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/MULTICLASS-per_class-precision.png)  
[MULTICLASS-per_class-recall](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/MULTICLASS-per_class-recall.png)  
[MULTICLASS-per_class-f1_score](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/MULTICLASS-per_class-f1_score.png)  

### Tablas de resultados


Se listan los resultados de las métricas de evaluación que se muestran arriba en las gráficas.  

#### Evaluación Multi-clase
  

##### Tabla de resultados para la evaluación global del modelo.
  

|Precisión|Recuperación|Score f1|
| :---: | :---: | :---: |
|0.29|0.31|0.29|
  

##### Tabla de resultados para la evaluación por clase del modelo.
  


Los resultados para las 10 categorías mejor evaluadas se muestran a continuación:  

|Categoría|Precisión|Recuperación|Score f1|
| :---: | :---: | :---: | :---: |
|Nea|1.0|1.0|1.0|
|Hptm|0.77|0.81|0.79|
|Froscm|0.69|0.81|0.75|
|Iosf b|0.44|0.81|0.57|
|Frgscm|0.75|0.77|0.76|
|Cotl|0.55|0.75|0.63|
|Hptl|0.83|0.75|0.79|
|Frgtb|0.63|0.74|0.68|
|Pac|0.86|0.72|0.78|
|Iosfm|0.41|0.7|0.52|


<details>

<summary>Para ver los valores de las métricas para las 50 categorías restantes, haga clic aquí.</summary>  
  

|Categoría|Precisión|Recuperación|Score f1|
| :---: | :---: | :---: | :---: |
|Froscb|0.82|0.67|0.74|
|Ffsa|0.61|0.67|0.64|
|Ofs|0.4|0.67|0.5|
|Gfs|0.56|0.66|0.61|
|Cotb|0.74|0.66|0.7|
|Hrtl|0.64|0.62|0.63|
|Ifs|0.72|0.6|0.66|
|Carr|0.68|0.6|0.64|
|Ioscsm|0.46|0.6|0.52|
|Iotb|0.56|0.56|0.56|
|Coscm|0.44|0.5|0.47|
|Frhtl|0.39|0.45|0.41|
|Cscb|0.41|0.43|0.42|
|Iff|0.2|0.41|0.27|
|Frosfm|0.49|0.37|0.42|
|Coscb|0.33|0.35|0.34|
|Ffs|0.5|0.32|0.39|
|Fa|0.18|0.29|0.22|
|Ifca|0.17|0.28|0.21|
|Cscl|0.4|0.2|0.27|
|Frotl|0.25|0.2|0.22|
|Hptb|0.12|0.19|0.15|
|Oa|0.2|0.15|0.17|
|Hfs|0.25|0.09|0.13|
|Cct|0.0|0.0|0.0|
|Csfb|0.0|0.0|0.0|
|Csfm|0.0|0.0|0.0|
|Ctb|0.0|0.0|0.0|
|Ctl|0.0|0.0|0.0|
|Frgasm|0.0|0.0|0.0|
|Frgsfsm|0.0|0.0|0.0|
|Frgtsm|0.0|0.0|0.0|
|Frhab|0.0|0.0|0.0|
|Froam|0.0|0.0|0.0|
|Froscsm|0.0|0.0|0.0|
|Frosfsm|0.0|0.0|0.0|
|Frotb|0.0|0.0|0.0|
|Frtsm|0.0|0.0|0.0|
|Ga|0.0|0.0|0.0|
|Gsfsm|0.0|0.0|0.0|
|Gtsm|0.0|0.0|0.0|
|Hac|0.0|0.0|0.0|
|Iavs|0.0|0.0|0.0|
|Icabd|0.0|0.0|0.0|
|Ioam|0.0|0.0|0.0|
|Ioscm|0.0|0.0|0.0|
|Iotm|0.0|0.0|0.0|
|Mscb|0.0|0.0|0.0|
|Psqb|0.0|0.0|0.0|
|Svsm|0.0|0.0|0.0|


</details>  
  

## Métricas utilizadas
  

### Conjunto de métricas multi-clase

- **Precisión**: La precision intenta responder la siguiente pregunta: ¿Qué proporción de predicciones positivas fue realmente correcta?
- **Recuperación (Recall)**: El recall intenta responder la siguiente pregunta: ¿Qué proporción de elementos positivos se identificó correctamente?
- **Score F1**: F1 es la media armónica de precisión y recuperación. Esta métrica penaliza los valores extremos y es una mejor métrica para evaluar problemas de distribución de clases desbalanceadas.
  

#### Configuración de la evaluación

- Tipo de promedio que se realiza sobre los datos: macro
  

## Análisis de resultados


A partir de los resultados se puede observar que varias de las categorías con mayor número de muestras tienen también un mejor rendimiento en la evaluación. Esto es en cierta manera normal ya que el modelo que se utilizó (Inception Resnet v2) aprende mediante ejemplos y mientras más muestras tenga de una clase, aprenderá mejor a distintguirla.
Otra característica que se observa es que algunas clases de talla grande como `Hptl` y `Hrtl` también tienen buenos resultados, esto debido a que los animales más grandes suelen ser más fácilmente identificados por los modelos.

## Conclusiones


A partir de lo anterior podemos concluir que el modelo tiene buen rendimiento para algunas clases, en especial aquellas con un alto número de muestras y algunas de talla grande, pero para otras tiene un desempeño bastante malo. 
Una forma de mejorar el comportamiento para estas clases es agregando más muestras de esas especies. Esto se puede llevar a cabo buscando esas especies en otras colecciones de fototrampas, y si no están etiquetadas a nivel de objeto (con recuadros o bouding boxes) como lo requiere esta metodología, esto se puede realizar de forma semi-automática utilizando un detector de objetos entrenado con colecciones de fototrampas, como el [Megadetector](https://github.com/microsoft/CameraTraps/blob/master/megadetector.md).

## Ejemplos de clasificación


Se pueden incluir ejemplos de las muestras mejor y peor clasificadas, así como las que están en la frontera entre dos clases.  

### Muestras mejor clasificadas
  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/d4b22627-32d8-8630-526a-cc8d11a2c132.jpg)  

|Etiqueta|Score|
| :---: | :---: |
|HPTL|99%|
|Carr|0%|
|FFSA|0%|
|CSCL|0%|
|IFS|0%|
  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/ae3b3ce6-27bd-43bb-239a-697e09dabe60.jpg)  

|Etiqueta|Score|
| :---: | :---: |
|HPTL|99%|
|CSCL|0%|
|Carr|0%|
|IFS|0%|
|FFSA|0%|
  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/11e53e7b-c838-b511-4937-9c8436ad9a1f.jpg)  

|Etiqueta|Score|
| :---: | :---: |
|HPTL|99%|
|Carr|0%|
|HRTL|0%|
|CSCL|0%|
|FrHTL|0%|
  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/158a9918-661c-6c78-3dba-7bf3405f2686.jpg)  

|Etiqueta|Score|
| :---: | :---: |
|FrGTB|99%|
|HPTM|0%|
|FrHTL|0%|
|CSCB|0%|
|IFS|0%|
  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/d9e0665c-5ce3-899b-124e-819dd1cf3f18.jpg)  

|Etiqueta|Score|
| :---: | :---: |
|HPTL|99%|
|IFS|0%|
|HRTL|0%|
|CSCL|0%|
|COSCB|0%|
  

### Muestras peor clasificadas
  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/7d0ee252-90d7-5b26-472d-7bba49cdc62e.jpg)  

|Etiqueta|Score|
| :---: | :---: |
|HRTL|99%|
|HPTL|0%|
|Carr|0%|
|HFS|0%|
|CTL|0%|
  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/110c5cef-d7db-1a71-429e-624faee30ab6.jpg)  

|Etiqueta|Score|
| :---: | :---: |
|HPTL|99%|
|HRTL|0%|
|FrHTL|0%|
|IFS|0%|
|HPTB|0%|
  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/d9a59a96-c45a-2517-773f-dd8fb9e92375.jpg)  

|Etiqueta|Score|
| :---: | :---: |
|FrGSCM|99%|
|IOSFM|0%|
|COTL|0%|
|COTB|0%|
|GFS|0%|
  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/c6378459-7cff-03ff-79ea-bf0ff8129f3c.jpg)  

|Etiqueta|Score|
| :---: | :---: |
|HRTL|99%|
|HPTL|0%|
|CTL|0%|
|COTB|0%|
|HPTB|0%|
  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/c68c48b5-ba71-71e1-8aa5-f000bfe5454b.jpg)  

|Etiqueta|Score|
| :---: | :---: |
|FrOSCB|99%|
|COSCB|0%|
|COTB|0%|
|MSCB|0%|
|CSCB|0%|
  

### Muestras en la frontera entre dos clases
  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/26800d96-e250-84c7-6f20-a16aaaaa483b.jpg)  

|Etiqueta|Score|
| :---: | :---: |
|HRTL|43%|
|HPTL|43%|
|OFS|2%|
|FrOSFM|2%|
|HPTB|1%|
  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/50ea8f60-0472-197e-5b0d-22c2f9aecb05.jpg)  

|Etiqueta|Score|
| :---: | :---: |
|HRTL|32%|
|HPTM|32%|
|HPTB|29%|
|FrOSCM|2%|
|COTB|0%|
  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/26665a68-1992-c014-b4c5-46b8bde88b57.jpg)  

|Etiqueta|Score|
| :---: | :---: |
|FFSA|17%|
|HPTM|17%|
|FFS|13%|
|IFF|12%|
|PAc|6%|
  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/8a5a3bd7-2c79-3658-2565-ddfe3e08323c.jpg)  

|Etiqueta|Score|
| :---: | :---: |
|COTL|42%|
|FrOSFM|42%|
|FrGSCM|14%|
|FrOSCB|0%|
|COTB|0%|
  
![](http://www.conabio.gob.mx/snmb_files/documentation/reports/images/automatic/pipeline_assets/60c90ad4-9583-4da2-a71a-da4bacce528f/17ca0b63-2394-7896-4389-30e362df31ea.jpg)  

|Etiqueta|Score|
| :---: | :---: |
|FFS|37%|
|GFS|37%|
|PAc|25%|
|IOSF B|0%|
|IFCA|0%|
  

# Apéndice
  

## Metodología de evaluación multi-clase



Los problemas de clasificación binaria se centran en una clase positiva que queremos detectar. Por el contrario, en un típico problema de clasificación multiclase, debemos clasificar cada muestra en una de N clases diferentes. Por ejemplo, podríamos querer clasificar una foto que contiene animales en alguna de varias categorías diferentes, que podrían ser parte de una jerarquía taxonómica.

Para este tipo de modelos se suele realizar el cálculo de las métricas descritas en la sección Clasificación binaria para cada una de las categorías, esto es, tomando una categoría como la clase positiva, y la suma de todas las demás como la clase negativa.

Por ejemplo, supongamos un modelo que trata de clasificar fotos que tienen animales de las siguientes clases: Mamíferos, Peces y Aves. Nuestro clasificador tiene que predecir qué animal se muestra en cada foto. Este es un problema de clasificación con N=3 clases.

Supongamos que la siguiente es la matriz de confusión después de clasificar 25 fotos:

![](https://i.imgur.com/YFDw4Q8.png)

De manera similar al caso binario, podemos definir precisión y recall para cada una de las clases. Por ejemplo, la precisión para la clase Mamífero es el número de fotos de mamíferos predichas correctamente (4) de todas las fotos de mamíferos predichas (4 + 3 + 6 = 13), lo que equivale a 4/13 = 30.8%. Entonces, solo alrededor de un tercio de las fotos que nuestro predictor clasifica como Mamífero son en realidad de mamíferos.
Por otro lado, el recall para la clase Mamífero es el número de fotos de mamíferos (4) pronosticadas correctamente del número de fotos de mamíferos reales (4 + 1 + 1 = 6), que es 4/6 = 66.7%. Esto significa que nuestro clasificador clasificó 2/3 de las fotos de mamíferos como Mamífero.

![](https://i.imgur.com/7kiKwkY.png) ![](https://i.imgur.com/Hqat3ni.png) ![](https://i.imgur.com/9uPsgxM.png)

De manera similar, podemos calcular la precisión y el recall de las otras dos clases: Peces y Aves. Para Peces, los números son 66.7% y 20.0% respectivamente. Para Aves, el número tanto de precisión como de recall es del 66,7%.
Los valores de score-F1 para cada categoría son: Mamífero = 42.1%, Peces = 30.8% y Aves = 66.7%.
Todas estas métricas se resumen en la siguiente tabla:

![](https://i.imgur.com/ItD679X.png)

### Macro-promedio

Ahora que tenemos los valores de las métricas para cada categoría, el siguiente paso es combinarlas para obtener las métricas para todo el clasificador. Hay varias formas de hacer esto y aquí analizaremos las dos más utilizadas. Empecemos con la más sencilla: calcular la media aritmética de la precisión y el recall y a partir de ellas el score-F1 global. Esta es llamada el Macro-promedio de la métrica, y se calcula:

![](https://i.imgur.com/edqb9hQ.png)

Una variante para calcular el **Macro-F1** que se suele preferir al evaluar datasets desbalanceados es con la media aritmética de los score-F1 individuales de todas las clases, ya que [se ha demostrado](https://arxiv.org/abs/1911.03347) que el otro método es excesivamente "benevolente" hacia clasificadores muy sesgados y puede dar valores engañosamente altos cuando se están evaluando este tipo de datasets, y ya que se suele usar el Macro-F1 con la intención de asignar igual peso a la clase más frecuente y a la menos frecuente, se recomienda utilizar esta última definición, que es significativamente más robusta hacia la distribución de este tipo de error.

### Micro-promedio

Otra forma de realizar el promedio de las métricas es con el micro-promedio, para el cuál se deben tomar todas las muestras juntas para hacer el cálculo de la precisión y el recall, y después con estos valores hacer el cálculo del score-F:

![](https://i.imgur.com/cX74dhp.png)

Para calcular la **micro-precisión** tenemos que sumar el total de TP y dividirlo entre el total de predicciones positivas. Para nuestro ejemplo, sumamos el total de elementos en la diagonal (color verde) para obtener TP=12. Después sumamos los falsos positivos, es decir, los elementos fuera de la diagonal, y obtenemos FP=13. Nuestra precisión es, por lo tanto, 12 / (12 + 13) = 48.0%.
Para calcular el **micro-recall** tenemos que sumar el total de falsos negativos (FN) para todas las clases, que al igual que para los FP, consiste en sumar todos los elementos fuera de la diagonal, con lo que se obtendría nuevamente FN=13 y el valor del micro-recall también es 12 / (12 + 13) = 48.0%.
Dado que el micro-recall es igual a la micro-precisión, entonces la media armónica, y por lo tanto el **micro-F1**, también será igual a este valor. Por tanto, en general se puede decir que:

![](https://i.imgur.com/Z1kZmPz.png)
                